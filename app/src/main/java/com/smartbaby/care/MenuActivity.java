package com.smartbaby.care;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.smartbaby.care.ui.tab1;
import com.smartbaby.care.ui.tab2;
import com.smartbaby.care.ui.tab3;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MenuActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.toolbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent login = new Intent(MenuActivity.this, Login_Page.class);
                switch (menuItem.getItemId()){
                    case R.id.item3:
                        Intent i = new Intent(MenuActivity.this, Halaman_Admin.class);
                        if (user == null){
                            startActivity(login);
                        } else {
                            startActivity(i);
                        }
                        break;
                    case R.id.item2:
                        Intent i2 = new Intent(MenuActivity.this, AkunSetting.class);
                        if (user == null){
                            startActivity(login);
                        } else {
                            startActivity(i2);
                        }
                        break;
                    case R.id.item1:
                        logout();
                        break;
                }
                return false;
            }
        });

        // kita set default nya Home Fragment
        loadFragment(new tab1());
        // inisialisasi BottomNavigaionView
        BottomNavigationView bottomNavigationView = findViewById(R.id.bn_main);
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()){
            case R.id.nav_home:
                fragment = new tab1();
                break;
            case R.id.nav_video:
                fragment = new tab2();
                break;
            case R.id.nav_forum:
                fragment = new tab3();
                break;
        }
        return loadFragment(fragment);
    }
    private void logout() {
        mAuth.signOut();
        Intent out = new Intent(MenuActivity.this, Login_Page.class);
        startActivity(out);
        finish();
    }
}
